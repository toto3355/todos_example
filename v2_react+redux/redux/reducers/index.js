export default function combineReducer(currentState = [], action) {
  switch (action.type) {
    case 'addTodo':
      return [
        ...currentState,
        {
          text: action.todo_text,
          completed: false
        }
      ]
    case 'toggleTodo':
      return currentState.map((todo, index) => {
        if (index === action.index) {
          return Object.assign({}, todo, {
            completed: !todo.completed
          })
        }
        return todo
      })
    default:
      return currentState;
  }
}