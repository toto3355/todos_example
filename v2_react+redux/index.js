import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import $ from 'jquery'
import TodosApp from './components/todosApp'
import { Provider } from 'react-redux'
import store from './redux/store/config'

function render(){
  ReactDOM.render(
    <Provider store={store}>
      <TodosApp/>
    </Provider>,
    document.getElementById('root')  
  )
}

render();

store.subscribe(render);
    