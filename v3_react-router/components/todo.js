import React, { Component } from 'react'
import {toggleTodo} from '../redux/actions/index'
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom' ;

class Todo extends Component {
  constructor(props) {
    super(props)
  }
  
  render() {
    const {todo, index, toggleTodo}= this.props;
    let className = "list-group-item";
      if(todo.completed){
        className += " disabled"
      }else{
        className += " list-group-item-info"
      }
    return (
    <div className={className} key={index}>
      {todo.text}
      <button className="badge done" onClick={e => {toggleTodo(index)}}>完成</button>
    </div>
    )
  }
}

const mapStateToProps = (state = [], ownProps) => {
  return {};
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    toggleTodo: (value) => dispatch(toggleTodo(value))
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Todo))