import React, { Component } from 'react'
import { connect } from 'react-redux'
import {toggleTodo} from '../redux/actions/index'
import Todo from './todo'

class TodoList extends Component {
   constructor(props) {
    super(props)
  }
  

  render() {
    const {todos, toggleTodo, match:{params:{status:status}}}= this.props;
    let completedStatus = false;

    if(status === 'done'){
     completedStatus = true;
    }

    return (
      <div className="list-group" id="todoList">
        {todos.map((todo, index) => {
          if(todo.completed == completedStatus)
          {
            return <Todo key={index} todo={todo} index={index} />
          }
        })}
      </div>
    )
  }
}

const mapStateToProps = (state = [], ownProps) => {
  return {todos: state};
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    toggleTodo: (value) => dispatch(toggleTodo(value))
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(TodoList)