import React, { Component } from 'react'
import {NavLink, Redirect} from 'react-router-dom'
export default class Nav extends Component {
  render() {  
    return (
      <ul className="nav nav-tabs">
        <li role="presentation">
          <NavLink  to="/">Add Todo</NavLink>
        </li>
        <li role="presentation">
          <NavLink  to="/list/done">Show done list</NavLink>
        </li>
        <li role="presentation">
          <NavLink  to="/list/undone">Show undone list</NavLink>
        </li>
      </ul>
    )
  }
}