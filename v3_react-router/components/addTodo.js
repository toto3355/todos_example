import React, { Component } from 'react'
import { connect } from 'react-redux'
import $ from 'jquery'
import {addTodo} from '../redux/actions/index'

class AddTodoForm extends Component{
  constructor(props) {
    super(props)
  }
  

  render(){
    let input;
    const {addTodo} = this.props;

    return  (
      <div  className="form-group">
      <h2>todos</h2>
      <form onSubmit={e => {
        e.preventDefault()
        if (!input.value.trim()) {
          return
        }
        addTodo(input.value)
        input.value = ''
      }}>
        <input ref={node => {
          input = node
        }} />
        <button type="submit">
          新增
        </button>
      </form>
    </div>
    )
  }
}

const mapStateToProps = (dispatch, ownProps) => {
  return {};
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    addTodo: (value) => dispatch(addTodo(value))    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddTodoForm)