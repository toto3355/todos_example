import React, { Component } from 'react'
import AddTodoForm from './addTodo'
import TodoList from './todoList'



export default class TodosApp extends Component {
  render() {
    return (
        <div className="row">
          <div className="col-md-5 col-md-offset-2">
            <h2>todos</h2>
            <AddTodoForm/>
            <TodoList />
          </div>
        </div>
    )
  }
}
