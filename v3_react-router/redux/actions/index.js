export const addTodo = (todo_text) => ({type: 'addTodo', todo_text});
export const toggleTodo = (index) => ({type: 'toggleTodo', index});

