import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import ReactDOM from 'react-dom'

import AddTodoForm from './components/addTodo'
import TodoList from './components/todoList'

import Nav from './components/nav'
import store from './redux/store/config'
import { Provider } from 'react-redux'



const Home = () => (
  <div>
    <h2>Home!!!</h2>
  </div>
)

const About = () => (
  <div>
    <h2>About!!!</h2>
  </div>
)

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div className="row">
        <div className="col-md-5 col-md-offset-2">
          <Nav />
          <Route exact path="/" component={AddTodoForm} />
          <Route path="/list/:status(done|undone)" component={TodoList}/>
        </div>
      </div>
    </Router>
  </Provider>
  , document.getElementById('root'));

